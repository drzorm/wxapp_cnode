let api = require('../../api/api');
let util = require('../../utils/util');
let accesstoken = require('../../config/api').accesstoken;
let app = getApp();

Page({
  data: {
    topics: [],
    limit: 20,
    loading: false,
  },
  onLoad: function() {
    this.fetchData();
    this.setData({
      userInfo: app.data.userInfo
    })
  },
  onPullDownRefresh: function() {
    this.fetchData(true);
  },
  onReachBottom: function() {
    this.fetchData();
  },
  fetchData: function(isRefresh) {
    let _this = this;
    if (_this.data.loading) return;
    _this.setData({
      loading: true
    });
    let topics = isRefresh ? [] : _this.data.topics;
    let page = isRefresh ? 1 : Math.ceil(topics.length / _this.data.limit) + 1;
    api.topic.getTopics({
      tab: 'all',
      page: page,
      limit: _this.data.limit
    }).then(res => {
      console.log(res);
      let data = res.data;
      let _d = data.data || [];
      if (data.success) {
        topics = topics.concat(_d.map(n => {
          n.tab = app.data.topicType[n.tab];
          n.create_at = util.dateFormat(n.create_at, 'yyyy-MM-dd hh:mm');
          return n;
        }))
        _this.setData({
          topics: topics,
          loading: false
        });
      }
      isRefresh && wx.stopPullDownRefresh();
    }, res => {
      _this.setData({
        loading: false
      });
      isRefresh && wx.stopPullDownRefresh();
    });
  },
  tapLogin: function() {
    let _this = this;
    wx.showActionSheet({
      itemList: ['扫码登录', '其他'],
      success: function(res) {
        if (res.cancel) return;
        switch (res.tapIndex) {
          case 0:
            wx.showModal({
              title: '提示',
              content: '小程序暂不提供扫码API',
              showCancel: false,
            });
            break;
          case 1:
            app.login({
              success: function() {
                wx.redirectTo({ url: 'index' });
              }
            });
            break;
          default:
            ;
        }
      }
    })
  },
  tapLogout: function() {
    let _this = this;
    wx.showActionSheet({
      itemList: ['个人中心', '退出'],
      success: function(res) {
        if (res.cancel) return;
        switch (res.tapIndex) {
          case 0:
            wx.navigateTo({ url: '../userInfo/userInfo?loginname=' + app.data.userInfo.loginname });
            break;
          case 1:
            app.logout({
              success: function() {
                wx.redirectTo({ url: 'index' });
              }
            });
            break;
          default:
            ;
        }
      }
    });
  }
})
