let api = require('../../api/api');
let util = require('../../utils/util');
let WxParse = require('../../wxParse/wxParse.js');
let app = getApp();

Page({
  data: {
    topic: {},
    wxParseData: []
  },
  onLoad: function(options) {
    if (typeof options.id === undefined) return;
    this.fetchData(options.id);
  },
  fetchData: function(id) {
    let self = this;
    if (self.data.loading) return;
    api.topic.getTopic({
      id: id,
      mdrender: false
    }).then(res => {
      console.log(res);
      let data = res.data;
      let _d = data.data || {};
      if (data.success) {
        _d.create_at = util.dateFormat(_d.create_at, 'yyyy-MM-dd hh:mm:ss');
        _d.replies = _d.replies.map(n => {
          n.create_at = util.dateFormat(n.create_at, 'yyyy-MM-dd hh:mm');
          return n;
        })
        self.setData({
          topic: _d,
        });
      }
      WxParse.wxParse('markdown', _d.content, self);
    }, res => {

    });
  },
  wxParseImgTap: function(e) {
    WxParse.wxParseImgTap(e, this);
  }
})
