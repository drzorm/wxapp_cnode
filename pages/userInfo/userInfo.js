let api = require('../../api/api');
let util = require('../../utils/util');
let app = getApp();

Page({
  data: {
    userInfo: {},
    current: 0
  },
  onLoad: function(options) {
    if (typeof options.loginname === undefined) return;
    this.fetchData(options.loginname);
  },
  fetchData: function(loginname) {

    let _this = this;
    if (_this.data.loading) return;

    api.user.detail(loginname).then(res => {
      console.log(res);
      let data = res.data;
      let _d = data.data || {};
      if (data.success) {
        _d.create_at = util.dateFormat(_d.create_at, 'yyyy-MM-dd hh:mm');
        _d.recent_replies = _d.recent_replies.map(n => {
          n.last_reply_at = util.dateFormat(n.last_reply_at, 'yyyy-MM-dd hh:mm');
          return n;
        })
        _d.recent_topics = _d.recent_topics.map(n => {
          n.last_reply_at = util.dateFormat(n.last_reply_at, 'yyyy-MM-dd hh:mm');
          return n;
        })
        _this.setData({
          userInfo: _d
        });
      }

    }, res => {

    });
  },
  tabchange: function(e) {
    let current = e.detail.current;
    if (current === undefined) {
      current = +e.target.dataset.current || 0;
    }
    this.setData({
      current: current
    })
  }
})
