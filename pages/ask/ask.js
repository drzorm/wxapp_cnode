let api = require('../../api/api');
let util = require('../../utils/util');
let app = getApp();

Page({
  data: {
    topics: [],
    limit: 20,
    loading: false
  },
  onLoad: function() {
    this.fetchData();
  },
  onPullDownRefresh: function() {
    this.fetchData(true);
  },
  onReachBottom: function() {
    this.fetchData();
  },
  fetchData: function(isRefresh) {
    let _this = this;
    if (_this.data.loading) return;
    _this.setData({
      loading: true
    });
    let topics = isRefresh ? [] : _this.data.topics;
    let page = isRefresh ? 1 : Math.ceil(topics.length / _this.data.limit) + 1;
    api.topic.getTopics({
      tab: 'ask',
      page: page,
      limit: _this.data.limit
    }).then(res => {
      console.log(res);
      let data = res.data;
      let _d = data.data || [];
      if (data.success) {
        topics = topics.concat(_d.map(n => {
          n.tab = app.data.topicType[n.tab];
          n.create_at = util.dateFormat(n.create_at, 'yyyy-MM-dd hh:mm');
          return n;
        }))
        _this.setData({
          topics: topics,
          loading: false
        });
      }
      isRefresh && wx.stopPullDownRefresh();
    }, res => {
      _this.setData({
        loading: false
      });
      isRefresh && wx.stopPullDownRefresh();
    });
  },
})
