let Promise = require('../utils/es6-promise.min').Promise;
let request = require('../utils/util').request;
let cnodeApiURI = require('../config/api').cnodeApiURI;

/**
 * [detail description]
 * @param  {[String]} loginname [description]
 * @return {[Promise]}           [description]
 */
const detail = (loginname) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: `${cnodeApiURI}/user/${loginname}`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

/**
 * [auth 验证accessToken的正确性]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const auth = (accesstoken) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'POST',
      data: {
        accesstoken: accesstoken
      },
      url: `${cnodeApiURI}/accesstoken`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

module.exports = {
  detail: detail,
  auth: auth,
}
