let topic = require('./topic');
let collect = require('./collect');
let reply = require('./reply');
let user = require('./user');
let message = require('./message');

module.exports = {
  topic: topic,
  collect: collect,
  reply: reply,
  user: user,
  message: message
}
