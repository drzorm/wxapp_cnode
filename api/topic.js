let Promise = require('../utils/es6-promise.min').Promise;
let request = require('../utils/util').request;
let cnodeApiURI = require('../config/api').cnodeApiURI;

/**
 * [getTopics 主题首页]
 * @param  {Number} options.page  [第几页]
 * @param  {Number} options.limit [分页数量]
 * @param  {String} options.tab   [栏目]
 * @return {[Object]}               [栏目数据]
 */
const getTopics = ({ page = 1, limit = 20, tab = 'all', mdrender = false } = {}) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'GET',
      data: {
        page: page,
        limit: limit,
        tab: tab,
        mdrender: mdrender,
      },
      url: `${cnodeApiURI}/topics`,
      showLoading: false,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

/**
 * [getTopic 主题详情]
 * @param  {[String]} id [description]
 * @return {[Promise]}    [description]
 */
const getTopic = ({ id, mdrender = false } = {}) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'GET',
      data: {
        mdrender: mdrender
      },
      url: `${cnodeApiURI}/topic/${id}`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

/**
 * [postTopics 新建主题]
 * @param  {[String]} options.accesstoken [description]
 * @param  {[String]} options.title       [description]
 * @param  {String} options.tab         [description]
 * @param  {Object} options.content     }            [description]
 * @return {[Promise]}                     [description]
 */
const addTopics = ({ accesstoken, title, tab, content } = {}) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'POST',
      data: {
        accesstoken: accesstoken,
        title: title,
        tab: tab,
        content: content
      },
      url: `${cnodeApiURI}/topics`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

/**
 * [updateTopics 编辑主题]
 * @param  {[String]} options.accesstoken [description]
 * @param  {[String]} options.topic_id    [description]
 * @param  {[String]} options.title       [description]
 * @param  {[String]} options.tab         [description]
 * @param  {Object} options.content     }            [description]
 * @return {[Promise]}                     [description]
 */
const updateTopics = ({ accesstoken, topic_id, title, tab, content } = {}) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'POST',
      data: {
        accesstoken: accesstoken,
        title: title,
        tab: tab,
        content: content
      },
      url: `${cnodeApiURI}/topics`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}


module.exports = {
  getTopics: getTopics,
  getTopic: getTopic,
  addTopics: addTopics,
  updateTopics: updateTopics,
}
