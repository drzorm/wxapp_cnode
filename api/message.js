let Promise = require('../utils/es6-promise.min').Promise;
let request = require('../utils/util').request;
let cnodeApiURI = require('../config/api').cnodeApiURI;

/**
 * [count 获取未读消息数]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const count = (accesstoken) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'GET',
      data: {
        accesstoken: accesstoken
      },
      url: `${cnodeApiURI}/message/count`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

/**
 * [messages 获取已读和未读消息]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const messages = (accesstoken) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'GET',
      data: {
        accesstoken: accesstoken,
        mdrender: true
      },
      url: `${cnodeApiURI}/messages`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}

/**
 * [markAll 标记全部已读]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const markAll = (accesstoken) => {
  let promise = new Promise((resolve, reject) => {
    request({
      method: 'POST',
      data: {
        accesstoken: accesstoken
      },
      url: `${cnodeApiURI}/message/mark_all`,
    }).then(res => {
      resolve(res);
    }, res => {
      reject(res);
    });
  });
  return promise;
}


module.exports = {
  count: count,
  messages: messages,
  markAll: markAll,
}
