//app.js
let api = require('./api/api');
let token = require('./config/api').accesstoken;
App({
  data: {
    userInfo: null,
    topicType: { all: '主页', ask: '问答', job: '招聘', share: '分享' },
  },
  onLaunch: function() {
    this.login();
  },
  onShow: function() {
    console.log('onShow');
  },
  onHide: function() {
    console.log('onHide');
  },
  getUserInfo: function(cb) {},
  login: function({ accesstoken, success, fail } = {}) {
    var _this = this;
    let _token = accesstoken || wx.getStorageSync('__ACCESSTOKEN__') || token;
    if (_token === undefined) return;
    api.user.auth(_token).then(res => {
      let data = res.data;
      let _d = data.data || {};
      if (data.success) {
        _this.data.userInfo = data;
        wx.setStorage({
          key: '__ACCESSTOKEN__',
          data: _token,
          success: function() {
            typeof success === 'function' && success();
          },
          fail: function() {
            wx.removeStorage({ key: '__ACCESSTOKEN__' });
            typeof fail === 'function' && success();
          }
        })
      } else {
        wx.showToast({
          title: data.error_msg,
          icon: 'success',
          duration: 2000
        })
      }
    }, res => {

      wx.showToast({
        title: data.errMsg,
        icon: 'success',
        duration: 2000
      })
    });
  },
  logout: function({ success } = {}) {
    var _this = this;
    wx.removeStorage({
      key: '__ACCESSTOKEN__',
      success: res => {
        _this.data.userInfo = null;
        wx.showToast({
          title: '退出登录',
          icon: 'success',
          duration: 2000,
          success: function() {
            typeof success === 'function' && success();
          }
        });
      }
    });
  }
})
